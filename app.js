var five = require("johnny-five");
var pixel = require("node-pixel");
var express = require('express');
var cookieParser = require('cookie-parser');
var onecolor = require('onecolor');
var bodyParser = require('body-parser');
var request = require('request');
var fs = require('fs');
var http = require('http');
var _ = require('underscore');
var PF = require('./pixel-fun.js');

var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var board = new five.Board();

var LED_STRIP_LENGTH = 24;

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(__dirname + '/public'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: {}
      });
});

var strip;
var minuteColor = onecolor("rgb(40,0,0)");
var minuteColorLightness = minuteColor.lightness();
var secondColor = onecolor("rgb(0,40,0)");
var secondColorLightness = secondColor.lightness();
var hourColor = onecolor("rgb(0,0,40)");
var hourColorLightness = hourColor.lightness();
var clockMode = 'hms3';

board.on("ready", function() {
    strip = new pixel.Strip({
        data: 6,
        length: LED_STRIP_LENGTH,
        board: this,
        controller: "FIRMATA"
    });

    function showTime(){
      var date = new Date();
      var hours = date.getHours()%12;
      var minutes = date.getMinutes();
      var seconds = date.getSeconds();
      var hoursPixel = +date.getHours() % 12;
      var minutesPixel = Math.floor(minutes / 5) + 12;
      var secondsPixel = Math.floor(seconds / 5) + 12;
      var hourAnticipation = date.getMinutes() > 0 ? date.getMinutes() / 60 : 0;
      var fiveMinuteAnticipation = (((minutes % 5)* 60) + seconds) / 300;
      var fiveSecondAnticipation = (seconds % 5) / 5;
      var myHourColor = onecolor(hourColor.hex());
      var myHourLightness = hourColorLightness * (1 - hourAnticipation);
      var myMinuteColor = onecolor(minuteColor.hex());
      var myMinuteLightness = minuteColorLightness * (1 - fiveMinuteAnticipation);
      var mySecondColor = onecolor(secondColor.hex());
      var mySecondLightness = secondColorLightness * (1 - fiveSecondAnticipation);

      strip.color("black");
      if (clockMode == 'hms2'){
        strip.pixel(secondsPixel).color(mySecondColor.lightness(mySecondLightness).css());
        strip.pixel(secondsPixel == 23 ? 12 : secondsPixel + 1 ).color(mySecondColor.lightness(secondColorLightness * fiveSecondAnticipation).css());
        strip.pixel(minutesPixel).color(myMinuteColor.lightness(myMinuteLightness).css());
        strip.pixel(minutesPixel == 23 ? 12 : minutesPixel + 1 ).color(myMinuteColor.lightness(minuteColorLightness * fiveMinuteAnticipation).css());
        strip.pixel(hoursPixel).color(myHourColor.lightness(myHourLightness).css());
        strip.pixel(hoursPixel == 11 ? 0 : hoursPixel + 1 ).color(myHourColor.lightness(hourColorLightness * hourAnticipation).css());
      }
      if (clockMode == 'hms1'){
        strip.pixel(secondsPixel).color(secondColor.css());
        strip.pixel(minutesPixel).color(minuteColor.css());
        strip.pixel(hoursPixel).color(myHourColor.lightness(myHourLightness).css());
      }
      if (clockMode == 'hms3'){
        for (var i = 12; ((i-12) * 5) <= seconds; i++){
          strip.pixel(i).color(secondColor.css());
        }
        for (var i = 0; (i * 5) <= seconds; i++){
          strip.pixel(i).color(secondColor.css());
        }
        for (var i = 12; ((i-12) * 5) <= minutes; i++){
          strip.pixel(i).color(minuteColor.css());
        }
        for (var i = 0; (i * 5) <= minutes; i++){
          strip.pixel(i).color(minuteColor.css());
        }
        for (var i = 0; i <= hours; i++){
          strip.pixel(i).color(hourColor.css());
        }
      }
      if (clockMode == 'hm2'){
        strip.pixel(minutesPixel).color(myMinuteColor.lightness(myMinuteLightness).css());
        strip.pixel(minutesPixel == 23 ? 12 : minutesPixel + 1 ).color(myMinuteColor.lightness(minuteColorLightness * fiveMinuteAnticipation).css());
        strip.pixel(hoursPixel).color(myHourColor.lightness(myHourLightness).css());
        strip.pixel(hoursPixel == 11 ? 0 : hoursPixel + 1 ).color(myHourColor.lightness(hourColorLightness * hourAnticipation).css());
      }
      if (clockMode == 'hm1'){
        strip.pixel(minutesPixel).color(minuteColor.css());
        strip.pixel(hoursPixel).color(myHourColor.lightness(myHourLightness).css());
      }
      if (clockMode == 'hm3'){
        for (var i = 12; ((i-12) * 5) <= minutes; i++){
          strip.pixel(i).color(minuteColor.css());
        }
        for (var i = 0; (i * 5) <= minutes; i++){
          strip.pixel(i).color(minuteColor.css());
        }
        for (var i = 0; i <= hours; i++){
          strip.pixel(i).color(hourColor.css());
        }
      }
      strip.show();

      setTimeout(showTime, 250);
    }

    strip.on("ready", function() {
        var pf = new PF(strip, LED_STRIP_LENGTH);

        function socketFunctions(socket) {
          function changeSeconds (data, callback) {
            secondColor = onecolor(data);
          }
          function changeMinutes (data, callback) {
            minuteColor = onecolor(data);
          }
          function changeHours (data, callback) {
            hourColor = onecolor(data);
          }
          function changeMode(data, callback){
            clockMode = data;
          }

          console.log('a user connected');

          socket.on('changeSeconds', changeSeconds);
          socket.on('changeMinutes', changeMinutes);
          socket.on('changeHours', changeHours);
          socket.on('changeMode', changeMode);
        }
        console.log("strip ready");

        io.on('connection', socketFunctions);
        server.listen(8888);
        showTime();
    });


});

module.exports = app;
