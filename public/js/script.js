$( document ).ready(function() {
  //UI Setup
  $('#tabs').tabs();

  var socket = io();

  // takes color information in the jQueryColorPicker's value's schema, and socket emits the color to node.
  var changeSeconds = function(color){
    socket.emit('changeSeconds', spectrumColorToRGB(color));
  };
  var changeMinutes = function(color){
    socket.emit('changeMinutes', spectrumColorToRGB(color));
  };
  var changeHours = function(color){
    socket.emit('changeHours', spectrumColorToRGB(color));
  };
  var changeMode = function(e) {
    socket.emit('changeMode', $(e.currentTarget).val());
  }

  var spectrumColorToRGB = function (color) {
    return 'rgb('+Math.floor(color._r)+', '+Math.floor(color._g)+', '+Math.floor(color._b)+')';
  };

  $('#secondsPickerHolder').spectrum({
    flat: true,
    move: $.throttle(32,changeSeconds),
    showButtons: false,
    color: "rgb(0,40,0)"
  });
  $('#minutesPickerHolder').spectrum({
    flat: true,
    move: $.throttle(32,changeMinutes),
    showButtons: false,
    color: "rgb(40,0,0)"
  });
  $('#hoursPickerHolder').spectrum({
    flat: true,
    move: $.throttle(32,changeHours),
    showButtons: false,
    color: "rgb(0,0,40)"
  });

  $('#modeSelector').change(changeMode);

});
